//
//  AddPasswordViewModel.swift
//  MykiLite
//
//  Created by Awwad on 8/14/19.
//  Copyright © 2019 myki. All rights reserved.
//

import Foundation

class AddPasswordViewModel {

  var password: Password!
  var fields: [Field] = []

  var numberOfCells: Int {
    return fields.count
  }

  init() {
    prepareFields()
  }

  func prepareFields() {
    let items: [NSDictionary] = [["title": "Header", "type": FieldTypes.header, "secure": false], ["title": "Nickname", "type": FieldTypes.text, "secure": false], ["title": "Username | Email", "type": FieldTypes.text, "secure": false], ["title": "Password", "type": FieldTypes.password, "secure": true], ["title": "Website", "type": FieldTypes.text, "secure": false]]

    for item in items {
      guard let title = item["title"] as? String, let type = item["type"] as? FieldTypes, let isSecure = item["secure"] as? Bool else { continue }
      fields.append(Field(title: title, type: type, isSecure: isSecure))
    }
  }

    func updatePassword (nickName: String, username: String, password: String, url: String) {
        self.password = Password(uuid: UUID().uuidString, nickname: nickName, username: username, password: password, url: url)
        database.createOrUpdate(model: self.password!, with: PasswordObject.init)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updatePasswordsTableView"), object: nil)
    }

}
