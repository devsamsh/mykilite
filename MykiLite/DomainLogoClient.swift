//
//  DomainLogoClient.swift
//  MykiLite
//
//  Created by Sami Sharafeddine on 10/1/19.
//  Copyright © 2019 myki. All rights reserved.
//

import Foundation
import UIKit

class DomainLogoClient {
    
    static let endpoint = URL(string: "https://logo.clearbit.com/")!
    
    static func getLogo (forDomain: String?, completion: @escaping (UIImage?, Error?) -> Void) {
        guard let domain = forDomain, !domain.isEmpty else {
            completion(nil, NSError(domain: "Error", code: 0, userInfo: nil))
            return
        }
        let requestUrl = endpoint.appendingPathComponent(domain)
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { (data, _, error) in
            if let error = error {
                completion(nil, error)
                return
            }
            if let data = data, let image = UIImage(data: data) {
                completion(image, nil)
            } else {
                completion(nil, NSError(domain: "Error", code: 0, userInfo: nil))
            }
        }
        task.resume()
    }
    
}
