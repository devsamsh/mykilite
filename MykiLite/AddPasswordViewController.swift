//
//  AddPasswordViewController.swift
//  MykiLite
//
//  Created by Awwad on 8/13/19.
//  Copyright © 2019 myki. All rights reserved.
//

import UIKit

class AddPasswordViewController: UITableViewController, UITextFieldDelegate {

  var viewModel: AddPasswordViewModel!

  override func viewDidLoad() {
    super.viewDidLoad()
    viewModel = AddPasswordViewModel()
    configureNavBar()
    configureView()
  }

  func configureNavBar() {
    self.title = "Add Password"
    self.navigationItem.largeTitleDisplayMode = .never
    self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(savePassword))
  }

  func configureView() {
    self.view.backgroundColor = .black
    self.tableView.separatorStyle = .none
    self.tableView.register(PasswordHeaderCellView.self, forCellReuseIdentifier: "headerCell")
    self.tableView.register(DetailCellView.self, forCellReuseIdentifier: "detailCell")
  }

  @objc func savePassword() {
    // Get nickname field from cell, check if empty, display error then return.
    guard let nickNameFieldCell = getNickNameCell(), !nickNameFieldCell.isEmpty() else {
        showErrorMessage(text: "Nickname field needs to have a value!")
      return
    }
    viewModel.updatePassword(nickName: nickNameFieldCell.detailTextField.text ?? "", username: getUserNameCell()?.detailTextField.text ?? "", password: getPasswordCell()?.detailTextField.text ?? "", url: getDomainCell()?.detailTextField.text ?? "")
    navigationController?.popViewController(animated: true)
  }

  @objc func textFieldChanged(_ textField: UITextField) {
    if let text = textField.text {
      viewModel.fields[textField.tag].value = text
    }
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    // Get domain field
    if let domainCell = getDomainCell(), textField == domainCell.detailTextField, !domainCell.isEmpty() {
      DomainLogoClient.getLogo(forDomain: textField.text) { [weak self] (image, error) in
        if error != nil {
          return
        }
        DispatchQueue.main.async {
          let header = self?.getHeaderCell()
          header?.itemImageView.image = image
        }
      }
    }
  }
  
  func showErrorMessage (text: String) {
    let alertVc = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
    let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
    alertVc.addAction(okAction)
    present(alertVc, animated: true, completion: nil)
  }

}
